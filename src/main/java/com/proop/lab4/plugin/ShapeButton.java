package com.proop.lab4.plugin;

import com.proop.lab4.shape.Shape;

import java.util.List;

import javax.swing.JButton;

public abstract class ShapeButton extends JButton {

    public abstract void setShapes(List<Shape> shapes);

    public abstract void setShape(Shape shape);

}
