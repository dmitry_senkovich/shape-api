package com.proop.lab4.plugin;

import java.util.List;

public interface ShapePlugin {

    List<Class<? extends ShapeButton>> getShapeButtons();

    List<Class<? extends ShapeMenu>> getShapeMenus();

}