package com.proop.lab4.plugin;

import javax.swing.JFrame;
import javax.swing.JMenu;

public abstract class ShapeMenu extends JMenu {

    public abstract void setParentFrame(JFrame parentFrame);

}
